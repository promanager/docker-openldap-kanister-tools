FROM registry.gitlab.com/silicon-hills/community/docker-openldap:0.0.1
RUN apt update -y
RUN apt install curl -y
RUN mkdir -p /tmp/kando && \
  cd /tmp/kando && \
  curl -Lo kanister.tar.gz https://github.com/kanisterio/kanister/releases/download/0.71.0/kanister_0.71.0_linux_amd64.tar.gz && \
  tar -xzvf kanister.tar.gz && \
  chmod +x ./kando && \
  mv kando /usr/local/bin/kando && \
  cd - && rm -rf /tmp/kando
RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/${kubectl_version}/bin/linux/amd64/kubectl \
    && chmod +x ./kubectl \
    && mv ./kubectl /usr/local/bin/kubectl
CMD ["/usr/bin/tail" "-f" "/dev/null"]